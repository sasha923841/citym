<?php
// Heading
$_['heading_title'] = 'So Filter Shop By';

// Text
$_['text_tax']      		= 'Ex Tax:';
$_['text_noproduct']      	= 'Has no item to show!';
$_['text_sale']      		= 'Sale';
$_['text_new']      		= 'New';

$_['text_search']      		= 'Поиск';
$_['text_price']      		= 'Цена';
$_['text_reset_all']      	= 'Сбросить';
$_['text_manufacturer']     = 'Производитель';
$_['text_subcategory']     	= 'Подкатегория';
$_['button_cart']     		= 'Добавить в корзину';
$_['button_compare']       = 'Compare';
$_['button_wishlist']       = 'wishlist';
$_['button_quickview']       = 'Быстрый просмотр';

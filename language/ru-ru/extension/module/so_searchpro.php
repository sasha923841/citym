<?php
// Text
$_['text_search']    	= 'Поиск';
$_['text_category_all'] = 'Все категории';
$_['text_tax']      	= 'Налог';
$_['text_price']      	= 'Цена';
$_['button_cart']       = 'Добавить в корзину';
$_['button_wishlist']       = 'Add to Wish List';
$_['button_compare']       = 'Add to Compare';

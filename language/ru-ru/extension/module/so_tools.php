<?php

$_['text_history']			= 'История';
$_['text_shopping_cart']	= 'Корзина покупок';
$_['text_register']			= 'Регистрация';
$_['text_account']			= 'Аккаунт';
$_['text_download']			= 'Загрузка';
$_['text_login']			= 'Логин';
$_['text_recent_products']	= 'Просмотренные товары';
$_['text_my_account']		= 'Мой аккаунт';
$_['text_new']				= 'Новинка';
$_['text_items_product']	= '<span class="text-color">%s Товар(ов)</span> в вашей корзине';
$_['text_items']     	    = '<span class="items_cart">%s</span><span class="items_cart2"> товар(ов) </span><span class="items_carts"> - %s </span> ';
$_['button_cart']			= 'Добавить в корзину';
$_['text_search']			= 'Поиск';
$_['text_all_categories']	= 'Все категории';
$_['text_head_categories']	= 'Категории';
$_['text_head_cart']		= 'Корзина';
$_['text_head_account']		= 'Аккаунт';
$_['text_head_search']		= 'Поиск';
$_['text_head_recent_view']	= 'Просмотренные';
$_['text_head_gotop']		= 'Go to Top';
$_['text_empty']               = 'Ваша корзина пуста!';
$_['text_no_content']               = 'Нет данных для отображения!';
<?php


// Quickview
$_['button_detail'] 		  		= 'Детали';
$_['text_overview']  = 'OverView';
$_['text_category_stock_quantity']  = 'Торопитесь! Осталось всего %s товар(ов) !';

// Home page
$_['text_more'] 			  		= 'Еще';
$_['text_shop']  			  		= 'Shopping now';
$_['text_shop_cart']		 		= 'Корзина';
$_['text_items_miss']        		= '%s';
$_['text_items']              		= '<span class="number">%s</span> <span class="text">товар(ов)</span> - <span class="number_2">%s</span>';
$_['text_contact']      	  		= 'Контакт';
$_['text_menu']  		 	  		= 'Меню';
$_['text_item_1']             		= 'Есть ';
$_['text_item_2']             		= ' в вашей корзине';
$_['text_item_3']            		 = ' товар(ов)';

$_['text_backtotop']          		= ' Вернуться вверх ';
$_['text_sub']                		= ' ALL NEWS OF OUR HURAMA	';
$_['text_stock']              		= ' Остаток ';
$_['text_stock1']             		= ' Остаток1 ';
$_['text_price_old']          		= ' Старая цена ';
$_['text_price_save']        	 	= ' Сохранить цену ';
$_['text_price_sale']         		= ' Скидочная цена ';
$_['text_viewdeals']          		= ' Посмотреть предложения ';
$_['text_instock']            		= ' В наличии ';
$_['text_show_cate']          		= ' Показать все категории ';
$_['button_quickview']        		= 'Добавить в быстрый просмотр';
$_['text_hotline']       	  		= 'Hot Line';
$_['text_compare2']       	  		= 'мои <b>сравнения</b>';
$_['text_compare']       	  		= 'Сравнение';
$_['text_sidebar']       	  		= 'Сайдбар';
$_['text_gridview']       	  		= 'Сетка:';
$_['text_alertaddtocart']       	= 'Успешно добавлено в корзину';

$_['text_neo1']       = 'Трендовые теги';
$_['text_neo2']       = 'Распродажа';
$_['text_neo3']       = 'Мода';
$_['text_neo4']       = 'Технологии';
$_['text_neo5']       = 'Фурнитура';


// Category page
$_['text_refine_more'] 				  		= 'Больше';
$_['text_refine_less'] 				  		= 'Меньше';
$_['text_product_orders'] 				  	= '%s продано. Осталось %s';
$_['text_product_orders_detail'] 				  	= '<span>Заказы </span> <span class="number"> (%s)  </span>';
$_['text_reviews']             = '(%s)';
$_['text_product_view'] 				  	= '%s раз';
$_['text_product_viewed'] 				  	= '%s раз';
$_['text_viewed'] 				  	= 'Просмотренные';

$_['text_viewdeals']  		  		= 'Смотреть все предложения';
$_['text_new'] 				  		= 'Новинка';
$_['text_sale'] 				  	= 'Скидка';
$_['text_quickview'] 				= 'Быстрый просмотр';
$_['text_labelDay'] 				= 'День%!d';
$_['text_labelHour'] 				= 'Час%!H';
$_['text_labelMin'] 				= 'Мин';
$_['text_labelSec'] 				= 'Сек';

$_['text_upsell'] 				  	= 'С этим товаром покупают';

// Product page
$_['text_sharethis']  		  = 'Поделиться';
$_['text_addwishlist']  		  = 'Добавить в закладки';
$_['text_addcompare']  		  = 'Добавить в сравнение';

$_['text_sidebar']        = 'Sidebar';
$_['text_size_chart']        = 'Размеры';
$_['tab_video']        = 'Видео';
$_['tab_shipping']        = 'Доставка';
$_['text_product_specifics']        = 'Спецификация';
$_['text_product_description']        = 'Описание Товара';
$_['text_or']        = 'или';

$_['show_more']       = 'Показать больше';
$_['show_less']       = 'Показать меньше';

$_['back_totop']       = 'Вверх';

$_['text_buynow']       = 'Быстрая покупка';




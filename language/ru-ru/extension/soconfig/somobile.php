<?php
// Text
$_['text_home']          = 'Главная';
$_['text_shopping_cart'] = 'Корзина';
$_['text_account']       = 'Кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Логин';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление';
$_['text_search']        = 'Поиска';
$_['text_cart']        = 'Корзина';
$_['text_all']           = 'Показать все';
$_['text_more']       = 'Еще';
$_['text_language']       = 'Языки';
$_['text_currency']       = 'Валюта';
$_['text_compare']       = 'Сравнение';
$_['text_itemcount']     = '<span class="items_cart">%s </span>';

$_['text_needhelp']      = 'Нужна помощь';
$_['text_emailus']       = 'Напишите нам';
$_['text_morecategory']       = 'Больше категорий';
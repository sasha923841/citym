<?php
// Heading  
//$_['heading_title']          = 'SOthemes Wish List';

// Text
$_['text_account']  = 'Аккаунт';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_title']             = 'Товар добавлен в закладки';
$_['text_failure']           = 'Неудача';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_exists']            = 'Some items are already listed on eBay so have been removed';
$_['text_success']           = 'Вы успешно добавили <a href="%s">%s</a> в <a href="%s">закладки</a>!';
$_['text_login']             = 'Вы должны <a href="%s">войти</a> или <a href="%s">cсоздать аккаунт</a> для добавления <a href="%s">%s</a> в ваши <a href="%s">закладки</a>!';
$_['text_remove']   	= 'Вы изменили свои закладки';
$_['text_empty']    	= 'Ваши закладки пусты';
?>
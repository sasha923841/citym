<?php

// Text
$_['text_title']             = 'Товар добавлен в Корзину';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = '<a href="%s">%s</a> добавлено в <a href="%s">корзину</a>!';
$_['text_items']     	     = '<span class="items_cart">%s</span><span class="items_cart2"> товар(ы)</span><span class="items_carts"> - %s </span> ';
$_['text_shop']  			  = 'Shopping now';
$_['text_shop_cart']		  = 'Корзина';

//QuickCart
$_['text_continue']  = 'Продолжить покупки';
$_['text_subtotal']  = 'Сумма заказа <span class="previewCartCheckout-price">%s</span> <span class="cart-popup-total">Ваша корзина содержит %s товар(ов)</span>';



// Error
$_['error_required']         = '%s required!';	

?>
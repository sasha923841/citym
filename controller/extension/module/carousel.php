<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		$data['banners'] = array();

                if($module == 0){
                    $this->load->model('extension/purpletree_multivendor/sellers');
                    $results = $this->model_extension_purpletree_multivendor_sellers->getSellers();
                    
                    foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['store_logo'])) {
				$data['banners'][] = array(
					'title' => $result['store_name'],
					'link'  => $this->url->link('extension/account/purpletree_multivendor/sellerstore/storeview', 'seller_store_id=' . $result['id'], true),
					'image' => $result['store_logo'] == "" ? $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']) : $this->model_tool_image->resize($result['store_logo'], $setting['width'], $setting['height'])
				);
			}else{
                            $data['banners'][] = array(
					'title' => $result['store_name'],
					'link'  => $this->url->link('extension/account/purpletree_multivendor/sellerstore/storeview', 'seller_store_id=' . $result['id'], true),
					'image' => $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height'])
				);
                        }
                    }
                    
                }else{
                    $results = $this->model_design_banner->getBanner($setting['banner_id']);
                    foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
                    }
                }

		$data['module'] = $module++;

		return $this->load->view('extension/module/carousel', $data);
	}
}